#Create an algorithm that reads a number and shows its double and, triple and the square root.

#Crie um algorítimo que leia um número e mostre o seu dobro e, triplo e a raiz quadrada.

n1 = int(input("Enter a number: "))
double = n1 * 2
triple = n1 * 3
root = n1 ** 2

print(">>Your number entered was {}<< \n >>Double the number is {}<< \n >>The triple is {}<< \n >>The square root is {}<<".format(n1, double, triple, root))

print('Your code worked sucessfully!')