# The same teacher from the previous challenge wants to draw the students' work order.
# Make a program that reads the names of the 4 students and shows the order drawn.

# O mesmo professor do desafio anterior quer sortear a ordem de trabalho dos alunos.
# Faça um programa que leia o nome dos 4 alunos e mostre a ordem sorteada.

from random import shuffle

a1 = input('Enter student name: ')
a2 = input('Enter another students name: ')
a3 = input('Enter another students name: ')
a4 = input('Enter another students name: ')
list = [a1, a2, a3, a4]
selected = shuffle(list)

print(f'The student select was: {selected}.')
print(list)