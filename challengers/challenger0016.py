#Create a program that reads any real number from the keyboard and shows its entire portion on the screen.

#Crie um programa que leia um número real qualquer pelo teclado e mostre na tela a sua porção inteira.
# EX: Digite um número: 6.17283. O número 6.17283 tem a porção inteira 6.

import math
number = float(input('Enter a number: '))
resul = math.trunc(number)

print(f'The entire number entered is:{resul}')

print('Your code ran successfully!')