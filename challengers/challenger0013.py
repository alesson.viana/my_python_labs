# Make an algorithm that reads the salary and shows your new salary, with a 15% increase.

#Faça um algoritmo que leia o salário e mostre seu novo salário, com 15% de aumento.

product = float(input('Enter your salary: '))
percentage = 15 / 100 * product
increase = product + percentage

print("Your new salary is: R${:.3f},00".format(increase))