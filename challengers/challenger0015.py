# Write a program that asks the number of kilometers traveled by a rental car and the number of days
# for which it was rented. Calculate the price to pay, knowing that the car costs R $ 60 per day and R $ 0.15 per km driven.

# Escreva um programa que pergunte a quantidade de Km percorridos por um carro alugado e a quantidade de dias
# pelos quais ele foi alugado. Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e R$0,15 por Km rodado.

km = float(input('Enter how many KMs were run: ')) * 0.15
days = int(input('Enter the number of days that were used: ')) * 60
result = km + days

print(f'The total amount to be paid is R${result:.2f}')



